
## iEmploy HR Management System

## PSEUDOCODE
ePay Human Resource Management System 

System Design Algorithm – 
1.	Employee_Func()
-	a.	Can View Pay slip from Start of Employment 
-	b.	Can request from Employer pertinent Employee Record such as follows:
-		i.	Certificate of Employment.
-		ii.	Copy of BIR, HDMF, PagIBIG Certificates (can also be viewed in PDF).
-		iii.	Request for Loan
-		iv.	Other Certificates
-	c.	Can View Leave incentives credits.
-	d.	(Optional) privilege to edit DTR func. 

2.	H_Resource_Func()
-	a.	Announcement Dashboard Add Privilege.
-	b.	(Optional)Has a privilege to upload DTR CSV from the system.
-	c.	Dashboard that can manage Employee_Func Pay Slip. With Overwrite privilege on Deductions.
-	d.	Dashboard that can view all Employee_Func request and sort based on date expected to deliver the 		 request.
-	e.	Has an override privilege on Leave Incentive with calendar markings.

3.	Account_Func
-	a.	Can View All Total Salary to be disbursed.

4.	Employer_Func 
-	a.	Announcement Dashboard Add Privilege.
-	b.	Can View All Employee Details
-	c.	Can View All Employee Attendance Records
-	d.	Can View All Total Salaries Disbursed 
-	e.	Can View All Employee Pay slip

Functionalities
View_Payslip();
Employee_Request();
COE_Req();
Gov_Rec)Req(String.compare(reqA,G.Cert);
Loan_Req();
Other_Req();
View_all_req();
Upload_csv();
Update_dtr();
Dtr_csv_upload();
Add_Payslip();
Edit_Payslip();
Archive_Payslip();
Update_leave();
Salary_disbursement();
Employee_rec();
Attendance_rec();
View_all_payslip();


## Status

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/BlackrockDigital/startbootstrap-sb-admin-2/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/startbootstrap-sb-admin-2.svg)](https://www.npmjs.com/package/startbootstrap-sb-admin-2)
[![Build Status](https://travis-ci.org/BlackrockDigital/startbootstrap-sb-admin-2.svg?branch=master)](https://travis-ci.org/BlackrockDigital/startbootstrap-sb-admin-2)
[![dependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-sb-admin-2/status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-sb-admin-2)
[![devDependencies Status](https://david-dm.org/BlackrockDigital/startbootstrap-sb-admin-2/dev-status.svg)](https://david-dm.org/BlackrockDigital/startbootstrap-sb-admin-2?type=dev)

## Download and Installation

To begin using this template, choose one of the following options to get started:

-   [Download the latest release on Start Bootstrap](https://startbootstrap.com/template-overviews/sb-admin-2/)
-   Install via npm: `npm i startbootstrap-sb-admin-2`
-   Clone the repo: `git clone https://github.com/BlackrockDigital/startbootstrap-sb-admin-2.git`
-   [Fork, Clone, or Download on GitHub](https://github.com/BlackrockDigital/startbootstrap-sb-admin-2)

## Usage

After installation, run `npm install` and then run `npm start` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.

### Gulp Tasks

-   `gulp` the default task that builds everything
-   `gulp watch` browserSync opens the project in your default browser and live reloads when changes are made
-   `gulp css` compiles SCSS files into CSS and minifies the compiled CSS
-   `gulp js` minifies the themes JS file
-   `gulp vendor` copies dependencies from node_modules to the vendor directory

You must have npm and Gulp installed globally on your machine in order to use these features. This theme was built using node v11.6.0 and the Gulp CLI v2.0.1. If Gulp is not running properly after running `npm install`, you may need to update node and/or the Gulp CLI locally.

## Bugs and Issues

Have a bug or an issue with this template? [Open a new issue](https://github.com/BlackrockDigital/startbootstrap-sb-admin-2/issues) here on GitHub or leave a comment on the [template overview page at Start Bootstrap](http://startbootstrap.com/template-overviews/sb-admin-2/).

## About

Start Bootstrap is an open source library of free Bootstrap templates and themes. All of the free templates and themes on Start Bootstrap are released under the MIT license, which means you can use them for any purpose, even for commercial projects.

-   <https://startbootstrap.com>
-   <https://twitter.com/SBootstrap>

Start Bootstrap was created by and is maintained by **[David Miller](http://davidmiller.io/)**.

-   <http://davidmiller.io>
-   <https://twitter.com/davidmillerskt>
-   <https://github.com/davidtmiller>

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2019 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-resume/blob/gh-pages/LICENSE) license.
